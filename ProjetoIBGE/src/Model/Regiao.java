/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

/**
 *
 * @author alexandre
 */
public class Regiao extends QuestionarioIBGE{
    
    private String CentroOeste;
    private String Sul;
    private String Sudeste;
    private String Norte;
    private String Nordeste;

    
    
    

    public String getCentroOeste() {
        return CentroOeste;
    }

    public void setCentroOeste(String CentroOeste) {
        this.CentroOeste = CentroOeste;
    }

    public String getSul() {
        return Sul;
    }

    public void setSul(String Sul) {
        this.Sul = Sul;
    }

    public String getSudeste() {
        return Sudeste;
    }

    public void setSudeste(String Sudeste) {
        this.Sudeste = Sudeste;
    }

    public String getNorte() {
        return Norte;
    }

    public void setNorte(String Norte) {
        this.Norte = Norte;
    }

    public String getNordeste() {
        return Nordeste;
    }

    public void setNordeste(String Nordeste) {
        this.Nordeste = Nordeste;
    }
    
    
}
