/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package funçoes;

/**
 *
 * @author alexandre
 */
import javax.swing.JOptionPane;

public class funçoes {
        public static void main(String[] args){
        //variaveis
        String sexo;
        String regiao;
        String etnia;
        int idade=0;
       
        int qtdEntrevistados = 0; //qtd de entrevistados
        double idMedEnt = 0.0; //Idade media dos entrevistados
       
        //sexo
        double medMas = 0.0;//media masculina
        double medFem = 0.0;//media femenian
        int qtdMas = 0; //qtd de entrevistados do sexo masculino
        int qtdFem = 0; //qtd de entrevistados do sexo feminino
        int qtdNordMas = 0; //qtd de masculino nordestino
        int qtdNordFem = 0; //qtd de femenino nordestino
        double perNeM =0; //percentual de nordestino masculino
        double perNeF =0; //percentual de nordestino femenino
       
        //idade
        double medIM = 0.0; //media da idade masculina
        double medIF = 0.0; //media da idade femenina
        int idTM = 0; //total das idades masculina
        int idTF = 0; //total da idade femenina
               
        //cor       
        int maiorGER = 0;
        int idNegro = 0; //acumula a idade do negro
        double medIdNegro = 0; //media da idade dos negros
        /**
         * vetor na posição Zero representa etinia branca
         * vetor na posição um representa etinia negro
         * vetor na posição dois representa etinia pardo
         * vetor na posição três representa etinia indio
         */
        int[] qtdENo = new int[4];
        int[] qtdENe = new int[4];
        int[] qtdESu = new int[4];
        int[] qtdESe = new int[4];
        int[] qtdECo = new int[4];

        //regiao
        int qtdNo = 0;
        int qtdNe = 0;
        int qtdSu = 0;
        int qtdSe = 0;
        int qtdCo = 0;

        String StrMNo = ""; //maior grupo etnico da regiao Norte
        String StrMNe = ""; //maior grupo etnico da regiao Nordeste
        String StrMSu = ""; //maior grupo etnico da regiao Sul
        String StrMSe = ""; //maior grupo etnico da regiao Sudeste
        String StrMCo = ""; //maior grupo etnico da regiao Centro-Oeste

        int menu; //menu das opções

        do {
            menu = Integer.parseInt(JOptionPane.showInputDialog("Informe a opção desejada:"
                    + "\n1-Insira um entrevistado"
                    + "\n2-Mostre a idade media dos entrevistados"
                    + "\n3-Mostre a idade media dos entrevistados(separadamente)"
                    + "\n4-Mostre qual foi o maior grupo etnico de cada região"
                    + "\n5-Mostre a idade média dos negros"
                    + "\n6-Mostre o percentual de homens e mulheres da população nordestina"
                    + "\n7-Sair"));
            switch (menu) {
                case 1:
                    qtdEntrevistados += 1; //qtd total de entrevistados 
                   
                    //entrada dos dados
                    sexo = JOptionPane.showInputDialog("Informe o sexo do entrevistado:");
                    idade = Integer.parseInt(JOptionPane.showInputDialog("Informe a idade do entrevistado:"));
                    regiao = JOptionPane.showInputDialog("Informe a regiao do entrevistado:"
                            + "\n(Ps: Escreva Centro Oeste: CENTRO-OESTE) ");
                    etnia = JOptionPane.showInputDialog("Informe a etnia do entrevistado:");
                   
                    //guarda algumas informações
                    //Analizando a etnia conforme a idade
                    if(sexo.equalsIgnoreCase("M")){
                        qtdMas += 1;
                        idTM += idade; //guarda o total das idades masculino
                        if(regiao.equalsIgnoreCase("NORDESTE")) qtdNordMas++;                  
                    }else{
                        qtdFem += 1;
                        idTF += idade;
                        if(regiao.equalsIgnoreCase("NORDESTE")) qtdNordFem++;
                    }
                    //Analizando a etnia conforme a regiao
                    //(PS: CASO A QTD DE PESSOAS COM A MESMA ETNIA FOREM IGUAIS GUARDARÁ A ULTIMA)
                    if(regiao.equalsIgnoreCase("NORTE")){ //norte
                        qtdNo++;
                        if (etnia.equalsIgnoreCase("BRANCO")) qtdENo[0] += 1;
                        if (etnia.equalsIgnoreCase("NEGRO")) qtdENo[1] += 1;
                        if (etnia.equalsIgnoreCase("PARDO")) qtdENo[2] += 1;
                        if (etnia.equalsIgnoreCase("INDIO")) qtdENo[3] += 1; 
                       
                        //verificar qual é o maior grupo de etnia
                        StrMNo = etnia; //maior é Branco
                        if(qtdENo[0] < qtdENo[1]) StrMNo = etnia; //maior é Negro
                        if(qtdENo[1] < qtdENo[2]) StrMNo = etnia; //maior é Pardo
                        if(qtdENo[2] < qtdENo[3]) StrMNo = etnia; //maior é Indio
                    }
                    if(regiao.equalsIgnoreCase("NORDESTE")){ //nordeste
                        qtdNe++;
                        if (etnia.equalsIgnoreCase("BRANCO")) qtdENe[0] += 1;
                        if (etnia.equalsIgnoreCase("NEGRO")) qtdENe[1] += 1;
                        if (etnia.equalsIgnoreCase("PARDO")) qtdENe[2] += 1;
                        if (etnia.equalsIgnoreCase("INDIO")) qtdENe[3] += 1;
                       
                        //verificar qual é o maior grupo de etnia
                        StrMNe = etnia;  //maior é Branco
                        if(qtdENe[0] < qtdENe[1]) StrMNe = etnia; //maior é Negro
                        if(qtdENe[1] < qtdENe[2]) StrMNe = etnia; //maior é Pardo
                        if(qtdENe[2] < qtdENe[3]) StrMNe = etnia; //maior é Indio   
                    }
                    if(regiao.equalsIgnoreCase("SUL")){ //sul
                        qtdSu++;
                        if (etnia.equalsIgnoreCase("BRANCO")) qtdESu[0] += 1;
                        if (etnia.equalsIgnoreCase("NEGRO")) qtdESu[1] += 1;
                        if (etnia.equalsIgnoreCase("PARDO")) qtdESu[2] += 1;
                        if (etnia.equalsIgnoreCase("INDIO")) qtdESu[3] += 1;  
                       
                        //verificar qual é o maior grupo de etnia
                        StrMSu = etnia;  //maior é Branco
                        if(qtdESu[0] < qtdESu[1]) StrMSu = etnia; //maior é Negro
                        if(qtdESu[1] < qtdESu[2]) StrMSu = etnia; //maior é Pardo
                        if(qtdESu[2] < qtdESu[3]) StrMSu = etnia; //maior é Indio
                    }
                    if(regiao.equalsIgnoreCase("SUDESTE")){ //sudeste
                        qtdSe++;
                        if (etnia.equalsIgnoreCase("BRANCO")) qtdESe[0] += 1;
                        if (etnia.equalsIgnoreCase("NEGRO")) qtdESe[1] += 1;
                        if (etnia.equalsIgnoreCase("PARDO")) qtdESe[2] += 1;
                        if (etnia.equalsIgnoreCase("INDIO")) qtdESe[3] += 1; 
                       
                        //verificar qual é o maior grupo de etnia
                        StrMSe = etnia;  //maior é Branco
                        if(qtdESe[0] < qtdESe[1]) StrMSe = etnia; //maior é Negro
                        if(qtdESe[1] < qtdESe[2]) StrMSe = etnia; //maior é Pardo
                        if(qtdESe[2] < qtdESe[3]) StrMSe = etnia; //maior é Indio
                    }
                    if(regiao.equalsIgnoreCase("CENTRO-OESTE")){ //centro-oeste
                        qtdCo++;
                        if (etnia.equalsIgnoreCase("BRANCO")) qtdECo[0] += 1;
                        if (etnia.equalsIgnoreCase("NEGRO")) qtdECo[1] += 1;
                        if (etnia.equalsIgnoreCase("PARDO")) qtdECo[2] += 1;
                        if (etnia.equalsIgnoreCase("INDIO")) qtdECo[3] += 1;
                       
                        //verificar qual é o maior grupo de etnia
                        StrMCo = etnia;  //maior é Branco
                        if(qtdECo[0] < qtdECo[1]) StrMCo = etnia; //maior é Negro
                        if(qtdECo[1] < qtdECo[2]) StrMCo = etnia; //maior é Pardo
                        if(qtdECo[2] < qtdECo[3]) StrMCo = etnia; //maior é Indio
                    }
                    //se for negro vai guardar a idade
                    if(etnia.equalsIgnoreCase("NEGRO")) idNegro += idade;              
                    break;
                case 2: //calcula a idade media dos entrevistados
                    //processamento
                    idMedEnt = (double)(idTF+idTM)/qtdEntrevistados;
                    //saida
                    JOptionPane.showMessageDialog(null,"Media idade: " + idMedEnt);
                    break;
                case 3: //calcula a idade media dos entrevistados(separadamente)
                    //processamento
                    medIM = (double)idTM/qtdMas; //media Idade Masculino
                    medIF = (double)idTF/qtdFem; //media Idade femenino
                    //saida
                    JOptionPane.showMessageDialog(null,"Media idade dos entrevistados:"
                            + "\nMasculino: " + medIM
                            + "\nFemenino: " + medIF);
                    break;
                case 4: //Maior grupo étnico de cada região
                    //saida
                    JOptionPane.showMessageDialog(null,"Maior grupo étnico de cada região:"
                            + "\nNorte: " + StrMNo
                            + "\nNordeste: " + StrMNe
                            + "\nSul: " + StrMSu
                            + "\nSudeste: " + StrMSe
                            + "\nCentro-Oeste: " + StrMCo);
                    break;
                case 5: //Idade média dos negros
                    //observação
                    //qtdENo[1] - quantidade de negros do norte
                    //qtdENe[1] - quantidade de negros do nordeste
                    //qtdESu[1] - quantidade de negros do sul
                    //qtdESe[1] - quantidade de negros do sudeste
                    //qtdECo[1] - quantidade de negros do centro-oeste
                   
                    //processamento
                    medIdNegro = (double)idNegro/(qtdENo[1]+qtdENe[1]+qtdESu[1]+qtdESe[1]+qtdECo[1]);
                    //saida
                    JOptionPane.showMessageDialog(null,"A idade média dos negros: " + medIdNegro);
                    break;
                case 6: //Porcentual de homens e mulheres na população nordestina
                    //processamento
                    perNeM = (double)qtdNordMas/(qtdNordMas+qtdNordFem); //percentual masc
                    perNeF = (double)qtdNordFem/(qtdNordMas+qtdNordFem); //percentual fem
                    //saida
                    JOptionPane.showMessageDialog(null,"Percentual de Nordestino por sexo:"
                            + "\nMasculino: " + (perNeM*100) + "%"
                            + "\nFemenino: " + (perNeF*100) + "%");
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null,"Fechando o programa...");
                    break;
                default:
                    JOptionPane.showMessageDialog(null,"Opção inválida!");
                    break;    
            } //fim do switch
        } while (menu != 7);
    } //fim do main
} //fim da classe
